MatHTJ2K
=====
This repository has been moved to https://github.com/osamu620/MatHTJ2K.
Updated version won't be available here.

MatHTJ2K is an implementation of JPEG 2000 Part 1 and Part 15 as defined in Rec. ITU-T T.800 | ISO/IEC 15444-1 and Rec. ITU-T T.814 | ISO/IEC 15444-15, written in MATLAB language.

## Description 

High Throughput JPEG 2000 (HTJ2K) is a new part of JPEG 2000 standard. The purpose of MatHTJ2K is to help a person who wants to develop an HTJ2K-based image compression system to understand the algorithms defined in HTJ2K. You can find more detailed information about HTJ2K on https://htj2k.com.

What you can do with MatHTJ2K are:

- to compress an image into a codestream which is compliant with JPEG 2000 Part 1 or Part 15.
- to decompress a codestream which is compliant with JPEG 2000 Part 1 or Part 15 into an image.
- to read/to write .jp2 and .jph file.